## A02
```
1. Napisati program koji učitava dužinu stranice kvadrata (a), a zatim računa i ispisuje njegov obim i površinu.
2. Napisati program koji učitava dužinu strane kocke (a), a zatim računa i ispisuje njenu zapreminu i površinu. 
3. Napisati program koji učitava dužine stranice pravougaonika (a i b), a zatim računa i ispisuje njegov obim i površinu te dužinu dijagonale. 
4. Napisati program koji učitava dužine strana dužine strana pravougaone prizme (a, b, c), a zatim računa i ispisuje njenu zapreminu i površinu. 
5. Napisati program koji učitava temperaturu izraženu u C, a zatim ispisuje koliko je to K.  
6. Napisati program koji učitava ugao izražen u stepenima, minutama i sekundama, a zatim ispisuje koliko je to ukupno stepeni.  
7. Napisati program koji učitava ugao izražen u stepenima, minutama i sekundama, a zatim ispisuje koliko je to radijana.  
8. Napisati program koji učitava ugao izražen u radijanima, a zatim ispisuje koliko je to stepeni.  
9. Napisati program koji učitava ugao izražen u radijanima, a zatim ispisuje koliko je to stepeni, minuta i sekundi (stepeni, minute i sekunde su cjelobrojni).
```